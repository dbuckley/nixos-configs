{ config, lib, pkgs, ... }:

{
  imports = if builtins.pathExists /etc/nixos/wifi_networks.nix then
    [
      ./wifi_networks.nix
    ]
  else
    [];
}
