{ config, lib, pkgs, ... }:

with lib;

let cfg = config.system.configUpdate; in

{
  options = {
    system.configUpdate = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = ''
          Hourly update of the nixos config from a git repo.
        '';
      };

      branch = mkOption {
        type = types.str;
        default = "master";
        example = "dev";
        description = ''
          Source repository to pull the config from.
        '';
      };
    };
  };

  config = {
    systemd.services.config-upgrade = {
      description = "NixOS Config update";

      restartIfChanged = false;
      unitConfig.X-StopOnRemoval = false;

      serviceConfig.Type = "oneshot";

      path = [
        pkgs.git
      ];

      script = ''
        cd /etc/nixos/ && git reset --hard && git pull --rebase origin ${cfg.branch}
      '';

      startAt = mkIf cfg.enable "12:00";
    };
  };
}
