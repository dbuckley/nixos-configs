{ lib, pkgs, config, ...}:
with lib;
let
  cfg = config.pkgGroups;
in {
  imports = [
    ./steam.nix
  ];

  options.pkgGroups = {
    enable = mkEnableOption ''
      Common grouping of packages seperated by free / non-free.
    '';
    withWorkstation = mkOption {
      type = types.bool;
      default = false;
      description = "Base workstation packages";
    };
    withUnfreeWorkstation = mkOption {
      type = types.bool;
      default = false;
      description = "Base unfree workstation packages";
    };
    withPhotography = mkOption {
      type = types.bool;
      default = false;
      description = "Photography applications";
    };
    withUnfreeChats = mkOption {
      type = types.bool;
      default = false;
      description = "Non free communication tools like slack";
    };
    withGames = mkOption {
      type = types.bool;
      default = false;
      description = "Games like multimc and steam (include Unfree)";
    };
    additional = mkOption {
      type = with types; listOf packages;
      default = [];
      description = "Additional packages to isntall.";
    };
  };

  config = lib.mkIf cfg.enable {
    nixpkgs.config.allowUnfree = (if cfg.withUnfreeChats ||
                                      cfg.withUnfreeWorkstation
        then true
        else false);

    environment.systemPackages = flatten [
      [ pkgs.curl pkgs.vim pkgs.git pkgs.man-pages pkgs.tmux ]
      (if cfg.withUnfreeWorkstation then [
        pkgs.bitwarden
        pkgs.bitwarden-cli
        pkgs.cmus
        pkgs.direnv
        pkgs.go
        pkgs.imv
        pkgs.pavucontrol
        pkgs.qutebrowser
        pkgs.tdesktop
      ] else [])
      (if cfg.withUnfreeWorkstation then [
        pkgs.spotify
      ] else [])
      (if cfg.withPhotography then [
        pkgs.digikam pkgs.darktable
      ] else [])
      (if cfg.withUnfreeChats then [
        pkgs.zoom-us pkgs.slack
      ] else [])
      (if cfg.withGames then [
        pkgs.multimc pkgs.discord
      ] else [])
    ];

    services.steam.enable = cfg.withGames;
  };
}
