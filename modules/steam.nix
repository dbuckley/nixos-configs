{ lib, pkgs, config, ...}:
with lib;
let
  cfg = config.services.steam;
in {
  options.services.steam = {
    enable = mkEnableOption "Steam Client";
    native = mkOption {
      type = types.bool;
      default = false;
      description = "use NixOS native libraries";
    };
  };

  config = lib.mkIf cfg.enable {
    hardware.opengl.driSupport32Bit = true;
    hardware.opengl.extraPackages32 = with pkgs.pkgsi686Linux; [ libva ];
    hardware.pulseaudio.support32Bit = true;
    
    nixpkgs.config.allowUnfree = true;
    environment.systemPackages = with pkgs; [
      (if cfg.native then
      (steam.override { nativeOnly = true; })
      else
      steam)
    ];
  };
}
