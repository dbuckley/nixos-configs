{ lib, pkgs, config, ...}:
with lib;
let

  kvToJson = x: if builtins.typeOf x == "list"
                  then builtins.toJSON x
                  else
                    if builtins.typeOf x == "bool"
                    then boolToString x
                    else toString x;
  kvToString = x: if builtins.typeOf x == "bool"
                   then boolToString x
                   else toString x;

  cfg = config.services.nas;

  btrfsShareCfg = share:
    (getAttrs [ "users" "group" "set_group" ] share);
    
  btrfsConfig = name:
    let
      share = btrfsShareCfg (getAttr name cfg.shares ); in
      "${name}: {" + (kvToString (
        map
          (key: "${key}: ${builtins.toJSON (getAttr key share)},")
          (attrNames share)
      )) + "},";

  configFile = pkgs.writeText "btrfs-decl.yml"
    ''
      root: ${cfg.root}
      subvolumes: {${kvToString (map btrfsConfig (attrNames cfg.shares))}}
    '';

  nfsExport = cfg: toString
  (
    (lib.mapAttrsToList
      (name: value: cfg.root + "/" + name + "\t" + value.exportOptions + "\n") cfg.shares
    ) ++ (lib.flatten (lib.mapAttrsToList (name: value:
      (map (v: cfg.root + "/" + name + "/" + v + "\t" + value.exportOptions + "\n"))
    value.users)
    cfg.shares))
  );

  usersFromOpts = opts:
  lib.genAttrs
    (lib.unique (lib.flatten
      (map (value: value.users) (lib.collect (x: x ? users) opts))
    ))
  (_: {});

in {
  options.services.nas = {
    enable = mkEnableOption "nas";
    exportNFS = mkEnableOption "Export over NFS";
    root = mkOption {
      type = types.str;
      default = "/";
      description = ''
        The root directory of the mounted BTRFS filesystem to export.
      '';
    };

    shares = mkOption {
      description = "Shares to export";
      type = with types; attrsOf (submodule {
        options = {
          users = mkOption {
            description = ''
              List of users to append to the export. Each user will have a
              subvolume of "share_name/user_name".
            '';
            type = types.listOf types.str;
            default = [];
          };

          group = mkOption {
            description = ''
              Group owner of the subvolume.
            '';
            type = types.str;
            default = "root";
          };

          set_group = mkOption {
            description = ''
              Set child subvolumes and user subvolumes to have the same group
              as defined by the "group" option.
            '';
            type = types.bool;
            default = false;
          };

          exportOptions = mkOption {
            description = ''
              NFS export options to use for the share and the user volumes
            '';
            type = types.str;
            default = "*(rw)";
          };
        };
      });
    };
  };

  config = lib.mkIf cfg.enable {
    environment.systemPackages = [ pkgs.bdecl ];

    # Ensure all users defined exist
    users.users = usersFromOpts cfg;

    # Create a service to create btrfs subvolumes on boot/start
    systemd.services.bdecl = {
      description = "Test btrfs config format";
      enable = true;

      restartIfChanged = false;
      unitConfig.X-StopOnRemoval = false;

      serviceConfig.Type = "oneshot";

      script = ''
        ${pkgs.bdecl}/bin/bdecl ${configFile}
      '';
    };

    services.nfs.server = lib.mkIf cfg.exportNFS {
      enable = true;
      exports = nfsExport cfg;
    };
  };
}
