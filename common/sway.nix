{ lib, pkgs, config, ...}:

let
  cfg = config.services.mySway;
in {
  hardware.pulseaudio.enable = true;

  fonts.fonts = [ pkgs.overpass pkgs.font-awesome pkgs.hack-font ];

  programs.sway = {
    enable = true;
    extraPackages = with pkgs; [
      alacritty
      grim
      imagemagick
      light
      mako
      swayidle
      swaylock
      (waybar.override { pulseSupport = true; })
      xwayland
    ];
  };
}
