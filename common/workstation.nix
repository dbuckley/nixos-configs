# Sets up workstation with sway and syncthing as well as my user account
{ config, pkgs, lib, ... }:

let
  vars = import ../vars.nix;
in {
  imports =
    [
      ./../modules
      ./sway.nix
      ./email.nix
    ];

  boot.kernel.sysctl = {
    "fs.inotify.max_user_watches" = 204800;
  };

  users.groups = {
    dbuckley = {
      gid = 82894;
    };
    cottage = {
      gid = 9292;
    };
  };

  users.users = {
    ${vars.users.u1.uname} = {
      uid = vars.users.u1.uid ;
      isNormalUser = true;
      home = vars.users.u1.home;
      description = vars.users.u1.name;
      extraGroups = [ "wheel" ] ++ vars.users.u1.groups;
    };
  };

  pkgGroups = {
    enable = true;
    withWorkstation = true;
  };

  services = {
    lorri.enable = true;
    sshd.enable = true;

    syncthing = {
      enable = true;
      configDir = "${vars.users.u1.home}/.config/syncthing";
      dataDir = "${vars.users.u1.home}/Desktop";
      user = "${vars.users.u1.uname}";
    };
  };
}
