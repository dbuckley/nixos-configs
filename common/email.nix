{ config, pkgs, lib, ... }:

{
  environment.systemPackages = with pkgs; [
    aerc
    protonmail-bridge
    gnome3.gnome-keyring
  ];
}
