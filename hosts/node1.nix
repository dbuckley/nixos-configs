{ config, pkgs, ... }:

{

  imports = [
      ../hardware-configuration.nix
      ../modules/pkgs.nix
    ];

  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only

  services.syncthing = {
    enable = true;
    configDir = "/etc/syncthing";
    dataDir = "/var/syncthing";
  };

  networking = {
    hostName = "node1";
    interfaces = {
      eno1.useDHCP = true;
      eno2.useDHCP = true;
      eno3.useDHCP = true;
      eno4.useDHCP = true;
    };
  };

  services.sshd.enable = true;
  environment.systemPackages = with pkgs; [
    curl vim git
  ];

  users.groups = {
    nixos-admin = {
      gid = 101909;
    };
    libvirtd = {
      gid = 67;
    };
  };

  users.users = {
    admin = {
      isNormalUser = true;
      home = "/home/admin";
      description = "Administrator";
      extraGroups = [ "wheel" "nixos-admin" "libvirtd" ];
      initialHashedPassword = "$6$cuLb3RU6mNEorZnW$fNS0zWvbCCJafr.BCNhc3L9rHGYLmZdtIz8s53ZY2ye0P9dEhex7DAemXXtQCfnrQyT.zrJroK74KsUVdOiBd0";
    };
    virt = {
      isNormalUser = true;
      home = "/home/virt";
      description = "Virt User";
      extraGroups = [ "libvirtd" ];
    };
  };

  virtualisation.libvirtd = {
    enable = true;
  };

  system.autoUpgrade.enable = true;
  system.stateVersion = "19.09"; # Did you read the comment?
}
