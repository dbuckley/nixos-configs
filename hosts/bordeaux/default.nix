# Base configuration for orleans laptop
{ config, pkgs, lib, ... }:

let
  vars = import ../../vars.nix;

in {

  imports =
    [
      ./../../modules
      ./hardware-configuration.nix
      ./../../common/workstation.nix
    ];

  boot.loader.systemd-boot.enable = true;

  virtualisation.docker.enable = true;

  networking = {
    hostName = "bordeaux";
    interfaces = {
      enp0s25.useDHCP = true;
      wlp3s0.useDHCP = true;
    };

    wireless.enable = true;
    wireless.networks = vars.wifiNetworks;
  };

  services = {
    printing.enable = true;
    printing.drivers = [ pkgs.hplipWithPlugin ];
  };

  users.groups.nixos-admin = {
    gid = 101909;
  };

  pkgGroups = {
    enable = true;
    withUnfreeWorkstation = true;
  };

  users.users = {
    admin = {
      isNormalUser = true;
      home = "/home/admin";
      description = "Administrator";
      extraGroups = [ "wheel" "nixos-admin" ];
      initialHashedPassword = "$6$cuLb3RU6mNEorZnW$fNS0zWvbCCJafr.BCNhc3L9rHGYLmZdtIz8s53ZY2ye0P9dEhex7DAemXXtQCfnrQyT.zrJroK74KsUVdOiBd0";
    };
  };

  system.stateVersion = "20.03";
}
