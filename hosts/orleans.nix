# Base configuration for orleans laptop
{ config, pkgs, lib, ... }:

let
  vars = import ../vars.nix;

in {
  imports =
    [
      ../hardware-configuration.nix
      ../common/workstation.nix
    ];

  boot.loader.systemd-boot.enable = true;

  networking = {
    hostName = "orleans";
    interfaces = {
      enp0s31f6.useDHCP = true;
      wlp4s0.useDHCP = true;
    };
  };

  users.groups.nixos-admin = {
    gid = 101909;
  };

  users.users = {
    admin = {
      isNormalUser = true;
      home = "/home/admin";
      description = "Administrator";
      extraGroups = [ "wheel" "nixos-admin" ];
      initialHashedPassword = "$6$cuLb3RU6mNEorZnW$fNS0zWvbCCJafr.BCNhc3L9rHGYLmZdtIz8s53ZY2ye0P9dEhex7DAemXXtQCfnrQyT.zrJroK74KsUVdOiBd0";
    };

    ${vars.users.u2.uname} = {
      isNormalUser = true;
      home = vars.users.u2.home;
      description = vars.users.u2.name;
      extraGroups = vars.users.u3.groups;
    };
  };

  environment.systemPackages = with pkgs; [
    krita
  ];

  services = {
    xserver = {
      enable = true;
      displayManager.gdm.enable = true;
      desktopManager.gnome3.enable = true;
    };
  };

  system.autoUpgrade.enable = true;
  system.stateVersion = "19.09"; # Did you read the comment?
}
