# Base configuration for orleans laptop
{ config, pkgs, lib, ... }:

let
  vars = import ../../vars.nix;

in {

  imports =
    [
      ./../../modules
      ./hardware-configuration.nix
      ../../common/workstation.nix
      ../../modules/config-update.nix

      # VSCode Live Share
      (fetchTarball "https://github.com/msteen/nixos-vsliveshare/tarball/master")
    ];

  hardware.bluetooth.enable = true;

  boot.loader.systemd-boot.enable = true;
  boot.kernelPackages = pkgs.linuxPackages_latest;

  networking = {
    hostName = "nice";
    interfaces = {
      wlp2s0.useDHCP = true;
    };

    wireless.enable = true;
    wireless.networks = vars.wifiNetworks;
  };

  fileSystems = {
    ${vars.users.u1.home + "/Video" } = {
      device = "tours.prod.evrt.cedit.io:/srv/data/video";
      fsType = "nfs";
      options = ["x-systemd.automount" "noauto"];
    };

    ${vars.users.u1.home + "/photos" } = {
      device = "tours.prod.evrt.cedit.io:/srv/data/photos/${vars.users.u1.uname}";
      fsType = "nfs";
      options = ["x-systemd.automount" "noauto"];
    };
  };

  virtualisation = {
    podman = {
      enable = true;
      dockerCompat = true;
    };
  };

  services = {
    vsliveshare = {
      enable = true;
      extensionsDir = "${vars.users.u1.home}/.vscode/extensions";
      nixpkgs = fetchTarball "https://github.com/NixOS/nixpkgs/tarball/61cc1f0dc07c2f786e0acfd07444548486f4153b";
    };
  };
  environment.systemPackages = [
    pkgs.vscode-with-extensions
    pkgs.discord
  ];

  pkgGroups = {
    enable = true;
    withUnfreeWorkstation = true;
    withUnfreeChats = true;
    withGames = true;
    additional = [
      pkgs.vscode
    ];
  };

  users.groups.nixos-admin = {
    gid = 101909;
  };

  users.users = {
    admin = {
      isNormalUser = true;
      home = "/home/admin";
      description = "Administrator";
      extraGroups = [ "wheel" "nixos-admin" ];
      initialHashedPassword = "$6$cuLb3RU6mNEorZnW$fNS0zWvbCCJafr.BCNhc3L9rHGYLmZdtIz8s53ZY2ye0P9dEhex7DAemXXtQCfnrQyT.zrJroK74KsUVdOiBd0";
    };
  };

  system.configUpdate.enable = true;
  system.autoUpgrade.enable = true;
  system.stateVersion = "19.09"; # Did you read the comment?

}
