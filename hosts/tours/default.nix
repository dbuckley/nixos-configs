{ config, pkgs, ... }:

let
  vars = import ../../vars.nix;
  custompkgs = import ../../pkgs {};

in {

  imports =
    [
      ./hardware-configuration.nix
      ../../modules
    ];

  boot.loader.systemd-boot.enable = true;

  services.syncthing = {
    enable = true;
    configDir = "/etc/syncthing";
    dataDir = "/var/syncthing";
  };

  networking = {
    hostName = "tours";
    interfaces = {
      eno1.useDHCP = true;
    };
  };

  services.sshd.enable = true;

  environment.systemPackages = with pkgs; [
    firefox
    pkgs.bdecl
  ];

  nixpkgs.overlays = [
    (_self: _super: {
      bdecl = custompkgs.bdecl;
    })
  ];

  users.groups = {
    nixos-admin = {
      gid = 101909;
    };
    cottage = {
      gid = 9292;
    };
  };

  users.users = {
    admin = {
      isNormalUser = true;
      home = "/home/admin";
      description = "Administrator";
      extraGroups = [ "wheel" "nixos-admin" ];
      initialHashedPassword = "$6$cuLb3RU6mNEorZnW$fNS0zWvbCCJafr.BCNhc3L9rHGYLmZdtIz8s53ZY2ye0P9dEhex7DAemXXtQCfnrQyT.zrJroK74KsUVdOiBd0";
    };
    ${vars.users.u1.uname} = {
      uid = vars.users.u1.uid ;
      isNormalUser = true;
      home = vars.users.u1.home;
      description = vars.users.u1.name;
      extraGroups = vars.users.u1.groups;
    };
  };

  networking.firewall.allowedTCPPorts = [ 111 2049 ];
  networking.firewall.allowedUDPPorts = [ 111 2049 ];

  services.nas = {
    root = "/srv/data";

    enable = true;
    exportNFS = true;
    shares = let
        defaultCfg = {
          group = "cottage";
          set_group = true;
          users = [ vars.users.u1.uname ];
          exportOptions = "*(rw)";
        };
      in {
      "backup" = defaultCfg;
      "ebooks" = defaultCfg;
      "music" = defaultCfg;
      "photos" = defaultCfg;
      "storage" = defaultCfg;
      "videos" = defaultCfg;
    };
  };

  system.stateVersion = "20.03";
}
