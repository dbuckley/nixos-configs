# Base configuration for orleans laptop
{ config, pkgs, lib, ... }:

let
  vars = import ../../vars.nix;
  unstableTar = fetchTarball https://nixos.org/channels/nixpkgs-unstable/nixexprs.tar.xz;

in {
  imports =
    [
      ./../../modules
      ./../../common/workstation.nix
      ./hardware-configuration.nix

      # VSCode Live Share
      (fetchTarball "https://github.com/msteen/nixos-vsliveshare/tarball/master")
    ];

  boot.loader.systemd-boot.enable = true;
  boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.kernelModules = [ "i2c-dev" "i2c-nct6775" ];

  networking = {
    hostName = "paris";
  };

  nixpkgs.config = {
    packageOverrides = pkgs: {
      unstable = import unstableTar {
        config = config.nixpkgs.config;
      };
    };
  };

  environment.systemPackages = with pkgs; [
    firefox
    python
    teams
    gvfs
    unstable.vscode-with-extensions
    unstable.displaycal
    unstable.digikam
    unstable.discord
    unstable.hydroxide
    unstable.openrgb
  ];
  services.syncthing.package = pkgs.unstable.syncthing;

  services.vsliveshare = {
    enable = true;
    extensionsDir = "$HOME/.vscode-oss/extensions";
    nixpkgs = fetchTarball "https://github.com/NixOS/nixpkgs/tarball/61cc1f0dc07c2f786e0acfd07444548486f4153b";
  };

  pkgGroups = {
    enable = true;
    withGames = true;
    withPhotography = true;
    withUnfreeChats = true;
    withUnfreeWorkstation = true;
  };

  users.groups.nixos-admin = {
    gid = 101909;
  };

  fileSystems = {
    ${vars.users.u1.home + "/Music"} =
    { device = "tours.prod.evrt.cedit.io:/srv/data/music";
      fsType = "nfs";
      options = ["x-systemd.automount" "noauto"];
    };

    ${vars.users.u1.home + "/videos" } = {
      device = "tours.prod.evrt.cedit.io:/srv/data/videos/${vars.users.u1.uname}";
      fsType = "nfs";
      options = ["x-systemd.automount" "noauto"];
    };

    ${vars.users.u1.home + "/photos" } = {
      device = "tours.prod.evrt.cedit.io:/srv/data/photos/${vars.users.u1.uname}";
      fsType = "nfs";
      options = ["x-systemd.automount" "noauto"];
    };

    ${vars.users.u2.home + "/Music"} = {
      device = "tours.prod.evrt.cedit.io:/srv/data/music/${vars.users.u2.uname}";
      fsType = "nfs";
      options = ["x-systemd.automount" "noauto"];
    };

    ${vars.users.u2.home + "/Videos" } = {
      device = "tours.prod.evrt.cedit.io:/srv/data/video/${vars.users.u2.uname}";
      fsType = "nfs";
      options = ["x-systemd.automount" "noauto"];
    };

    ${vars.users.u2.home + "/Photos" } = {
      device = "tours.prod.evrt.cedit.io:/srv/data/photos/${vars.users.u2.uname}";
      fsType = "nfs";
      options = ["x-systemd.automount" "noauto"];
    };
  };

  virtualisation.docker.enable = true;

  users.users = {
    admin = {
      isNormalUser = true;
      home = "/home/admin";
      description = "Administrator";
      extraGroups = [ "wheel" "nixos-admin" ];
      initialHashedPassword = "$6$cuLb3RU6mNEorZnW$fNS0zWvbCCJafr.BCNhc3L9rHGYLmZdtIz8s53ZY2ye0P9dEhex7DAemXXtQCfnrQyT.zrJroK74KsUVdOiBd0";
    };

    ${vars.users.u2.uname} = {
      isNormalUser = true;
      home = vars.users.u2.home;
      description = vars.users.u2.name;
      extraGroups = vars.users.u2.groups;
    };
  };

  location.provider = "geoclue2";

  services = {
    xserver = {
      enable = true;
      displayManager.gdm.enable = true;
      desktopManager.gnome3.enable = true;
    };
    redshift = {
      enable = true;
      package = pkgs.redshift-wlr;
    };
  };

  services.gvfs.enable = true;
  services.udisks2.enable = true;

  system.stateVersion = "19.09";
}
