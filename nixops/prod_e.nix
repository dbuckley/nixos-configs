{
  network.description = "Production Environment E";

  defaults = {
    networking.search = [
      "prod.evrt.cedit.io"
      "wifi.evrt.cedit.io"
    ];
  };

  tours = { config, pkgs, ... }: {
    imports = [
      ./../hosts/tours
    ];
  };
}
