let
  nixPathStable = "https://channels.nixos.org/nixos-20.03/nixexprs.tar.xz";
  nixPathSmall = "nixpkgs=https://nixos.org/channels/nixos-20.03/nixexprs.tar.xz";
  nixPathUnstable = "nixpkgs=https://nixos.org/channels/nixos-unstable/nixexprs.tar.xz";
in {
  network.description = "workstations";

  defaults = {
    networking.search = [
      "search prod.evrt.cedit.io"
      "wifi.evrt.cedit.io"
    ];
  };

  bordeaux = { config, pkgs, ... }: {
    imports = [
      ./../hosts/bordeaux
    ];

    nix.nixPath = [
      nixPathUnstable
    ];
  };

  paris = { config, pkgs, ... }: {
    imports = [
      ./../hosts/paris
    ];

    nix.nixPath = [
      nixPathStable
    ];
  };

  nice = { config, pkgs, ...}: {
    imports = [
      ./../hosts/nice
    ];

    nix.nixPath = [
      nixPathUnstable
    ];
  };
}
