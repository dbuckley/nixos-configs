#/bin/sh -e
# Default btrfs installation to an NVME drive using UEFI

_DISK="/dev/nvme0n1"
_DISK_PART="/dev/nvme0n1p"

default_efi.sh $_DISK $_DISK_PART
