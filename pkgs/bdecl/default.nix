{ stdenv, meson, ninja, pkgconfig, fetchgit, libyaml, btrfs-progs }:

stdenv.mkDerivation rec {
  name = "bdecl-${version}";
  version = "0.2.0";

  goPackagePath = "git.sr.ht/~dbuckley/btrfs-decl";

  src = fetchgit {
    url = "https://git.sr.ht/~dbuckley/bdecl";
    rev = "v${version}";
    sha256 = "1v7fllica9cbc8z0fam3ll9sm1pf244kns9fdsnvwi9rmbnw2liw";
  };

  nativeBuildInputs = [
    pkgconfig meson ninja
  ];

  buildInputs = [
    libyaml btrfs-progs
  ];

  meta = with stdenv.lib; {
    homepage = https://git.sr.ht/~waffle_ethics/bdecl;
    description = "Use yaml to define BTRFS subvolumes.";
    license = licenses.mpl20;
  };
}
