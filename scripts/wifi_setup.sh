#!/bin/sh -e

export NIX_WIFI_CONF="/etc/nixos/wifi_networks.nix"

add_network() {
    local _conf=$1
    shift
    local _ssid=$1
    shift
    local _psk=$1
    shift

    if [ ! -f $_conf ]; then
        cat >$_conf <<EOF
{ config, lib, pkgs, ... }:

{
  networking.wireless.enable = true;
  networking.wireless.networks = {
    $_ssid = { psk = "$_psk"; };
  };
}
EOF
    else
        local _tmp=$(mktemp)
        local _new_line="$_ssid = { psk = \"$_psk\"; };"

        if grep -q $_ssid $_conf; then
            sed -e "s/^\s*$_ssid = .*/    $_new_line/" $_conf >$_tmp
        else
            sed "/networking.wireless.networks = {/a\
\    $_new_line" \
                $_conf >$_tmp
        fi

        mv $_tmp $_conf
    fi
}

export -f add_network

usage='Usage wifi_setup.sh [SSID] [PASSWORD]'
if [ -z $1 ] || [ -z $2 ]; then
    echo 1>&2 You must specify ssid and password
    echo 1>&2
    echo 1>&2 $usage
    exit 1
fi

add_network $NIX_WIFI_CONF $1 $2
