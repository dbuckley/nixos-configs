#/bin/sh -e
# Install bordeaux

__partition() {
	local __DISK=$1; shift
	local __DISK_PART=$1; shift

	parted $__DISK --script -- mklabel msdos
	parted $__DISK --script -- mkpart primary 1MiB -8MiB
	parted $__DISK --script -- mkpart primary linux-swap -8MiB 100%

	mkfs.xfs -f -L nixos ${__DISK_PART}1
	mkswap -L swap ${__DISK_PART}2
}


__mount() {
	local __DISK_PART=$1; shift

	mount /dev/disk/by-label/nixos /mnt
	swapon ${__DISK_PART}2
}

export -f __partition
export -f __mount
