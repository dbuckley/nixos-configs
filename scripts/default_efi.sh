#/bin/sh -e
# Install efi system

__partition() {
	local __DISK=$1; shift
	local __DISK_PART=$1; shift

	echo "Wiping $__DISK..."
	dd if=/dev/zero of=$__DISK bs=512KiB count=2000 > /dev/null 2>&1

	parted $__DISK --script -- mklabel gpt
	parted $__DISK --script -- mkpart primary 512MiB -8GiB
	parted $__DISK --script -- mkpart primary linux-swap -8GiB 100%
	parted $__DISK --script -- mkpart ESP fat32 1MiB 512MiB
	parted $__DISK --script -- set 3 boot on

	mkfs.btrfs -f -L nixos ${__DISK_PART}1
	mkfs.fat -F 32 -n boot ${__DISK_PART}3
	mkswap -L swap ${__DISK_PART}2

	# Ensure time for /dev/disk/by-label to populate
	sleep 1
}


__mount() {
	local __DISK_PART=$1; shift

	mount /dev/disk/by-label/nixos /mnt || return 1
	mkdir -p /mnt/boot || return 1
	mount /dev/disk/by-label/boot /mnt/boot || return 1
	swapon ${__DISK_PART}2
}

__umount() {
	local __DISK_PART=$1; shift

	umount -l /mnt/boot > /dev/null 2>&1
	umount -l /mnt > /dev/null 2>&1
	swapoff ${__DISK_PART}2 > /dev/null 2>&1
}

__partition $1 $2
if ! __mount $2; then
	__umount $2
	exit 1
fi
