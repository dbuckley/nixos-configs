#!/bin/sh

# returns the first nvme or sda (in that order) disk
__disk() {
	local __disk=$(find /dev/ -iname 'nvme[0-9]n*' | sort -n | head -n 1)
	if [ -z $__disk ]; then
		local __disk=$(find /dev/ -iname 'sd[a-z]*' | sort -n | head -n 1)
	fi
	echo $__disk
}
export -f __disk

__disk_part() {
	local __disk=$(find /dev/ -iname 'nvme[0-9]n*' | sort -n | head -n 1)
	if [ -z $__disk ]; then
		local __disk=$(find /dev/ -iname 'sd[a-z]*' | sort -n | head -n 1)
	else
		local __disk=${__disk}p
	fi
	echo $__disk
}
export -f __disk_part
