#!/bin/sh
# Update the host list

set -e

HOSTNAME=$(hostname)
HOSTLIST="$(echo $0 | rev | cut -d '/' -f2- | rev)/../host_list.txt"

__line() {
    local __hostname=$1
    shift
    local __address="$(ip a | grep 'link/e' | awk '{print $2}' | sort | uniq | tr '\n' ' ')"
    echo "$__hostname $__address" | sed 's/\s$//g'
}

__commit() {
    local _hostlist=$1
    shift
    local _hostname=$1
    shift

    git add $_hostlist
    git commit -m "updated host list for host \"$_hostname\""
}

touch $HOSTLIST
if grep -q $HOSTNAME $HOSTLIST; then
    _hostlist=$(mktemp)
    sed "s|$HOSTNAME.*|$(__line $HOSTNAME)|" $HOSTLIST >$_hostlist
    if diff $HOSTLIST $_hostlist >/dev/null; then
        rm $_hostlist
        exit 0
    fi

    mv $_hostlist $HOSTLIST
else
    echo $(__line $HOSTNAME) >>$HOSTLIST
fi

__commit $HOSTLIST $HOSTNAME
echo file updated and commited
